﻿using NUnit.Framework;

namespace HostsFileUpdater.Tests
{
    [TestFixture]
    public class ProgramTests
    {
        static void Main(string[] args)
        {
        }

        [Test]
        public void ValidOption_ForValidParameters_ExpectTrue()
        {
            Assert.IsTrue(Program.ValidOption("1"));
            Assert.IsTrue(Program.ValidOption("2"));
            Assert.IsTrue(Program.ValidOption("3"));
            Assert.IsTrue(Program.ValidOption("4"));
        }

        [Test]
        public void ValidOption_ForInvalidParameters_ExpectFalse()
        {
            Assert.IsFalse(Program.ValidOption("5"));
            Assert.IsFalse(Program.ValidOption("0"));
        }

        [Test]
        public void ReadHostFileEntries_ExpectHostsFileEntryList()
        {
            var hostsFileEntries = Program.ReadHostFileEntries();
            
            Assert.IsNotNull(hostsFileEntries);
        }
    }
}
