﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;

namespace HostsFileUpdater
{
    public static class Program
    {
        // Path to hosts file read from config, if null default is supplied
        private static readonly string HostsFilePath = ConfigurationManager.AppSettings["HostsFilePath"] ?? @"C:\Windows\System32\drivers\etc\hosts";

        /// <summary>
        /// Application entry point
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            // Display welcome message
            WriteLine("Welcome to host file updater", true);

            // Prompt user for input
            GetUserInput();
        }

        /// <summary>
        /// Displays program options and accepts user input
        /// </summary>
        private static void GetUserInput()
        {
            // Display the options
            WriteLine("Select an option and press enter:", true);
            WriteLine("1. View current entries");
            WriteLine("2. Add a new entry");
            WriteLine("3. Delete an existing entry");
            WriteLine("4. Exit program", true);

            // Get user input
            var selectedOption = Console.ReadLine();

            // If invalid input, try again
            while (!ValidOption(selectedOption))
            {
                Console.WriteLine();
                WriteLine("That's not a valid option, please try again", true);
                selectedOption = Console.ReadLine();
            }

            // Switch depending on input
            switch (Convert.ToInt32(selectedOption))
            {
                case 1:
                    ListEntries();
                    break;
                case 2:
                    AddEntry();
                    break;
                case 3:
                    DeleteEntry();
                    break;
                case 4:
                    Environment.Exit(0);
                    return;
            }

            GetUserInput();
        }

        /// <summary>
        /// Lists current hosts file entries
        /// </summary>
        private static void ListEntries()
        {
            WriteLine("Getting current host file entries...", true);

            var hostEntries = ReadHostFileEntries();

            if (hostEntries == null)
                return;

            WriteLine(string.Format("Found {0} entries", hostEntries.Count), true);

            foreach (var hostEntry in hostEntries)
                Console.WriteLine("IP: {0} Hostname: {1}", hostEntry.IP, hostEntry.Hostname);

            GetUserInput();
        }

        /// <summary>
        /// Adds a new entry to hosts file
        /// </summary>
        private static void AddEntry()
        {
            var currentEntries = ReadHostFileEntries();

            Console.WriteLine();

            WriteLine("Please enter IP address to map", true);
            var ip = Console.ReadLine();

            Console.WriteLine();

            WriteLine("Please enter hostname to map", true);
            var hostname = Console.ReadLine();

            currentEntries.Add(new HostsFileEntry(ip, hostname));

            using (var hostFile = File.Open(HostsFilePath, FileMode.Truncate))
            {
                using (var sw = new StreamWriter(hostFile))
                {
                    foreach (var entry in currentEntries)
                        sw.WriteLine(string.Format("{0} {1}", entry.IP, entry.Hostname));
                }
            }

            GetUserInput();
        }

        /// <summary>
        /// Deletes an existing entry from hosts file
        /// </summary>
        private static void DeleteEntry()
        {
            GetUserInput();
        }

        /// <summary>
        /// Reads all the current hosts file entries
        /// </summary>
        /// <returns>
        /// A list of HostsFileEntry
        /// </returns>
        public static List<HostsFileEntry> ReadHostFileEntries()
        {
            var hostFileEntries = new List<HostsFileEntry>();

            try
            {
                using (var hostFile = File.Open(HostsFilePath, FileMode.Open))
                {
                    using (var sr = new StreamReader(hostFile))
                    {
                        while (!sr.EndOfStream)
                        {
                            try
                            {
                                var currentLine = sr.ReadLine();

                                // If current line is null / empty, starts with # (is a comment) or doesn't contain a space - continue
                                if (string.IsNullOrEmpty(currentLine) || currentLine.StartsWith("#") || !currentLine.Contains(" "))
                                    continue;

                                var ip = currentLine.Substring(0, currentLine.IndexOf(" ")).Trim();
                                var hostname = currentLine.Substring(currentLine.IndexOf(" ")).Trim();

                                hostFileEntries.Add(new HostsFileEntry(ip, hostname));
                            }
                            catch
                            {
                                continue;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                WriteLine("Following exception ocurred:", true);
                WriteLine(e.Message, true);
                WriteLine("Please check your config and try again");
                return null;
            }

            return hostFileEntries;
        }

        /// <summary>
        /// Validates user's selected option
        /// </summary>
        /// <param name="selectedOption">
        /// The selected option
        /// </param>
        /// <returns>
        /// True of false
        /// </returns>
        public static bool ValidOption(string selectedOption)
        {
            return (selectedOption == "1" || selectedOption == "2" || selectedOption == "3" || selectedOption == "4");
        }

        /// <summary>
        /// Console write line helper
        /// </summary>
        /// <param name="w">
        /// The string to be written
        /// </param>
        /// <param name="addLineBreak">
        /// Switch to control whether line break is added after string
        /// </param>
        private static void WriteLine(string w, bool addLineBreak = false)
        {
            Console.WriteLine(w);

            if (addLineBreak) 
                Console.WriteLine();
        }
    }
}
