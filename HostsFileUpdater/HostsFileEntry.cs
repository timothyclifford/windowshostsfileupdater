﻿namespace HostsFileUpdater
{
    public class HostsFileEntry
    {
        public string IP { get; set; }
        public string Hostname { get; set; }

        public HostsFileEntry(string ip, string hostname)
        {
            IP = ip;
            Hostname = hostname;
        }
    }
}
